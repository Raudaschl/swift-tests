//
//  DetailViewController.swift
//  todoList
//
//  Created by Adrian Raudaschl on 23/11/2015.
//  Copyright © 2015 Adrian Raudaschl. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var imgObject: UIImageView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var setLabel: String?
    var imgURL: NSURL?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
            setLabelFunc()
    }

    private func setLabelFunc(){

        if let url = imgURL{
           spinner.startAnimating()
            mainLabel.text = setLabel
            let qos = QOS_CLASS_USER_INITIATED
            dispatch_async(dispatch_get_global_queue(qos, 0), { () -> Void in
                 let imgData = NSData(contentsOfURL: url)
                dispatch_async(dispatch_get_main_queue()) {
                
                    if imgData != nil {
                        self.imgObject.image = UIImage(data: (imgData)!)
                       self.spinner.stopAnimating()
                    }
                
                }
            })
           
            
          
        
        }
   
    }



}
