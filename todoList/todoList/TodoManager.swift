//
//  AddTodo.swift
//  todoList
//
//  Created by Adrian Raudaschl on 22/11/2015.
//  Copyright © 2015 Adrian Raudaschl. All rights reserved.
//

import UIKit
import CoreData

class TodoManager {
    
    //single access point
    
    static let sharedInstance = TodoManager()
    
    
    
    var todoArray = [Todo]()
    
    let context: NSManagedObjectContext!
    
    var count: Int {
        get {
            return todoArray.count
        }
    
    }
    
    func todoAtIndex (index: Int) -> Todo {
    
        return todoArray[index]
    }
    
    func addTodo(name: String, date: NSDate)
    {
        let todo = NSEntityDescription.insertNewObjectForEntityForName("Todo", inManagedObjectContext: context) as! Todo
        
        todo.name = name
        todo.completed = false
        todo.date = date.timeIntervalSince1970
        
        todoArray.append(todo)
        
        saveContext()
    }
    
    func removeTodo(index: Int)
    {
        context.deleteObject(todoAtIndex(index))
        todoArray.removeAtIndex(index)
        saveContext()
    }
    
    func saveContext(){
    
        do {
            try context.save()
        
        } catch let error as NSError{
            
            print("Error saving context \(error)")
        
        }
        
    }
    
    func fetchTodos(){
    
        let fetchRequest = NSFetchRequest(entityName: "Todo")
        
        do {
            //try//
            let results = try context.executeFetchRequest(fetchRequest)
            todoArray = results as! [Todo]
            
        } catch let error as NSError {
            print(error)
        }
    }
    
    private init() {
    
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        context = appDelegate.managedObjectContext
        fetchTodos()
    
    }

}
