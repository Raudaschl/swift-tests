//
//  ViewController.swift
//  todoList
//
//  Created by Adrian Raudaschl on 22/11/2015.
//  Copyright © 2015 Adrian Raudaschl. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchResultsUpdating, UISearchBarDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var searchController: UISearchController!
    
//    var dataArray = ["cat", "dog", "Kitten", "Medikidz", "iam46"]
    
    
    var filteredArray = [Todo]()
    
    var shouldShowSearchResults = false
    
    
    
    //MARK: Cell Function
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    configureSearchController()
        definesPresentationContext = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        tableView.reloadData()
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     

     
        if shouldShowSearchResults {
            return filteredArray.count
        }
        else {
            
            if section == 1 {
                return 1
            }
            
            return TodoManager.sharedInstance.count
        }
        
        
        
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCellWithIdentifier("AddNewCell")!
            return cell
        }
    
    let cell = tableView.dequeueReusableCellWithIdentifier("tableCell", forIndexPath: indexPath) as UITableViewCell
        
        let formatter = NSDateFormatter()
        formatter.dateStyle = .FullStyle
        
        
        
        if shouldShowSearchResults {
            cell.textLabel?.text = filteredArray[indexPath.row].name
            let date = NSDate(timeIntervalSince1970: filteredArray[indexPath.row].date)
            
            cell.detailTextLabel?.text = formatter.stringFromDate(date)
            
            
        }
        else {
            cell.textLabel?.text = TodoManager.sharedInstance.todoAtIndex(indexPath.row).name
            
            let date = NSDate(timeIntervalSince1970: TodoManager.sharedInstance.todoAtIndex(indexPath.row).date)
            
            cell.detailTextLabel?.text = formatter.stringFromDate(date)

        }
    return cell
        
    }
    

    //If cell is clicked
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        let currentCell = tableView.cellForRowAtIndexPath(indexPath)!
        
        print(currentCell.textLabel!.text)
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        
        if indexPath.section == 1{
            return false
        }
        return true
    }
    
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            
            TodoManager.sharedInstance.removeTodo(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Left)
        }
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
       
        
        if let identifier = segue.identifier{
            switch identifier {
                case "detailedView":
                    
                    let dvc = segue.destinationViewController as? DetailViewController
                    if let rowSelected = tableView.indexPathForSelectedRow?.row{
                        let rowTitle = TodoManager.sharedInstance.todoAtIndex(rowSelected)
                        
                        dvc!.title = rowTitle.name
                        dvc!.setLabel = rowTitle.name
                        dvc?.imgURL = NSURL(string: "https://ssl.webpack.de/lorempixel.com/400/600/")
                    }
            default:
                return
            }
        }
    }
    
    
    //MARK: Search Function
    

    
    func configureSearchController() {
        // Initialize and perform a minimum configuration to the search controller.
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search here..."
        searchController.searchBar.delegate = self
        searchController.searchBar.sizeToFit()
        
        // Place the search bar view to the tableview headerview.
        tableView.tableHeaderView = searchController.searchBar
    }
    
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        shouldShowSearchResults = true
        tableView.reloadData()
    }
    
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        shouldShowSearchResults = false
        tableView.reloadData()
    }

    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        if !shouldShowSearchResults {
            shouldShowSearchResults = true
            tableView.reloadData()
        }
        
        searchController.searchBar.resignFirstResponder()
    }
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        let searchString = searchController.searchBar.text
        
        // Filter the data array and get only those countries that match the search text.
        filteredArray = TodoManager.sharedInstance.todoArray.filter({ (country) -> Bool in
            let countryText: NSString = country.name!
            
            return (countryText.rangeOfString(searchString!, options: NSStringCompareOptions.CaseInsensitiveSearch).location) != NSNotFound
        })
        
        // Reload the tableview.
        tableView.reloadData()
    }


}

