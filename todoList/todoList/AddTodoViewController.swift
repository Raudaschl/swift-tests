//
//  AddTodoViewController.swift
//  todoList
//
//  Created by Adrian Raudaschl on 26/11/2015.
//  Copyright © 2015 Adrian Raudaschl. All rights reserved.
//

import UIKit

class AddTodoViewController: UIViewController, UITextFieldDelegate {
    
    var date: NSDate!

    @IBOutlet weak var nameText: UITextField!
 
    @IBOutlet weak var dateText: UITextField!
    
    
    @IBAction func saveButtonPress(sender: AnyObject) {
        print(date)
        
       TodoManager.sharedInstance.addTodo(nameText.text!, date: date)
        print(nameText.text)
        
         self.dismissViewControllerAnimated(true, completion: {});
    }
    
    @IBAction func cancelButtonPress(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: {});
    }
    
    //MARK: Touch events
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        date = NSDate()
        
        displayDate(date)
    }
    

    //MARK: Text field delegate methods
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
        
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if textField == dateText {
            let datePicker = UIDatePicker()
            textField.inputView = datePicker
            datePicker.addTarget(self, action: "datePickerChanged:", forControlEvents: .ValueChanged)
        }
        
        
    }
    
    func datePickerChanged (sender: UIDatePicker) {
        displayDate(sender.date)
        
    }
    
    func displayDate(date: NSDate){
    
        let formatter = NSDateFormatter()
        formatter.dateStyle = .FullStyle
        dateText.text = formatter.stringFromDate(date)
        self.date = date
    }
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        nameText.delegate = self
        dateText.delegate = self
        
        date = NSDate()
        
    }

 

}
