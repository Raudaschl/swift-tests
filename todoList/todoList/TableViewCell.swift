//
//  TableViewCell.swift
//  todoList
//
//  Created by Adrian Raudaschl on 22/11/2015.
//  Copyright © 2015 Adrian Raudaschl. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
