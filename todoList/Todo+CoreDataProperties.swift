//
//  Todo+CoreDataProperties.swift
//  todoList
//
//  Created by Adrian Raudaschl on 04/12/2015.
//  Copyright © 2015 Adrian Raudaschl. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Todo {

    @NSManaged var completed: Bool
    @NSManaged var name: String?
    @NSManaged var date: NSTimeInterval

}
