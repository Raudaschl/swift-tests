//
//  ViewController.swift
//  mapkitChurch
//
//  Created by Adrian Raudaschl on 28/11/2015.
//  Copyright © 2015 Adrian Raudaschl. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController,  MKMapViewDelegate {
    
    @IBOutlet weak var theMapView: MKMapView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        let latitude: CLLocationDegrees = 48.399193
        let longitude: CLLocationDegrees = 9.993341
        
        let latDelta: CLLocationDegrees = 0.01
        let lonDelta: CLLocationDegrees = 0.01
        
        let theSpan: MKCoordinateSpan = MKCoordinateSpanMake(latDelta, lonDelta)
        
        let churchLocation: CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
        
        let theRegion: MKCoordinateRegion = MKCoordinateRegionMake(churchLocation, theSpan)
        
        theMapView.setRegion(theRegion, animated: true)
        
        
        
        let theUlmMinsterAnnotation = MKPointAnnotation()
        
        theUlmMinsterAnnotation.coordinate = churchLocation
        
        theUlmMinsterAnnotation.title = "Ulm Minster"
        theUlmMinsterAnnotation.subtitle = "Tallest church"
        
        theMapView.addAnnotation(theUlmMinsterAnnotation)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

